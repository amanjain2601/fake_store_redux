import { INPUT_PRICE } from './priceTypes';

export const inputPrice = (inputValue) => {
  return {
    type: INPUT_PRICE,
    payLoad: inputValue,
  };
};
