import { INPUT_PRICE } from './priceTypes';

const initialState = {
  input: '',
};

const priceReducer = (state = initialState, action) => {
  switch (action.type) {
    case INPUT_PRICE:
      return {
        input: action.payLoad,
      };

    default:
      return state;
  }
};

export default priceReducer;
