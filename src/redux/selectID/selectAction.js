import { SELECT_ID } from './selectTypes';

export const selectID = (inputValue) => {
  return {
    type: SELECT_ID,
    payLoad: inputValue,
  };
};
