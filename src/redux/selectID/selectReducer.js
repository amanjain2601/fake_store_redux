import { SELECT_ID } from './selectTypes';

const initialState = {
  input: '',
};

const selectReducer = (state = initialState, action) => {
  switch (action.type) {
    case SELECT_ID:
      return {
        input: action.payLoad,
      };

    default:
      return state;
  }
};

export default selectReducer;
