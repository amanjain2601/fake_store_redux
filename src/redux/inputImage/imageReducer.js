import { INPUT_IMAGE } from './imageTypes';

const initialState = {
  input: '',
};

const imageReducer = (state = initialState, action) => {
  switch (action.type) {
    case INPUT_IMAGE:
      return {
        input: action.payLoad,
      };

    default:
      return state;
  }
};

export default imageReducer;
