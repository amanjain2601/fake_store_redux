import { INPUT_IMAGE } from './imageTypes';

export const inputImage = (inputValue) => {
  return {
    type: INPUT_IMAGE,
    payLoad: inputValue,
  };
};
