import { INPUT_DESCRIPTION } from './descriptionTypes';

const initialState = {
  input: '',
};

const descriptionReducer = (state = initialState, action) => {
  switch (action.type) {
    case INPUT_DESCRIPTION:
      return {
        input: action.payLoad,
      };

    default:
      return state;
  }
};

export default descriptionReducer;
