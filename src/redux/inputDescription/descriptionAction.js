import { INPUT_DESCRIPTION } from './descriptionTypes';

export const inputDescription = (inputValue) => {
  return {
    type: INPUT_DESCRIPTION,
    payLoad: inputValue,
  };
};
