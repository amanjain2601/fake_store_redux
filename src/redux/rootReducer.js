import { combineReducers } from 'redux';
import fetchReducer from './fetch/fetchReducer';
import titleReducer from './inputTitle/titleReducer';
import priceReducer from './inputPrice/priceReducer';
import descriptionReducer from './inputDescription/descriptionReducer';
import categoryReducer from './inputCategory/categoryReducer';
import imageReducer from './inputImage/imageReducer';
import selectReducer from './selectID/selectReducer';

const rootReducer = combineReducers({
  fetch: fetchReducer,
  title: titleReducer,
  price: priceReducer,
  description: descriptionReducer,
  image: imageReducer,
  category: categoryReducer,
  select: selectReducer,
});

export default rootReducer;
