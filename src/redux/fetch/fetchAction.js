import {
  ADD_TO_LIST,
  FETCH_PRODUCT_ERROR,
  FETCH_PRODUCT_REQUEST,
  FETCH_PRODUCT_SUCCESS,
  UPDATE_LIST,
} from './fetchTypes';

import axios from 'axios';

export const fetchProductsRequest = () => {
  return {
    type: FETCH_PRODUCT_REQUEST,
  };
};

export const fetchProductSuccess = (users) => {
  return {
    type: FETCH_PRODUCT_SUCCESS,
    payLoad: users,
  };
};

export const fetchProductsError = (error) => {
  return {
    type: FETCH_PRODUCT_ERROR,
    payLoad: error,
  };
};

export const addProductToList = (object) => {
  return {
    type: ADD_TO_LIST,
    payLoad: object,
  };
};

export const updateProductList = (object) => {
  return {
    type: UPDATE_LIST,
    payLoad: object,
  };
};

export const fetchProducts = () => {
  return (dispatch) => {
    dispatch(fetchProductsRequest());
    axios
      .get('https://fakestoreapi.com/products')
      .then((response) => {
        dispatch(fetchProductSuccess(response.data));
      })
      .catch((error) => {
        dispatch(fetchProductsError(error.message));
      });
  };
};

export const addProduct = (object) => {
  return (dispatch) => {
    axios
      .post('https://fakestoreapi.com/products', object)
      .then(function (response) {
        dispatch(addProductToList(response.data));
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const updateProduct = (object, id) => {
  return (dispatch) => {
    axios
      .put(`https://fakestoreapi.com/products/${id}`, object)
      .then((response) => {
        dispatch(updateProductList(response.data));
      })
      .catch((error) => {
        console.error('There was an error!', error);
      });
  };
};
