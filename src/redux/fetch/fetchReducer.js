import {
  ADD_TO_LIST,
  FETCH_PRODUCT_ERROR,
  FETCH_PRODUCT_REQUEST,
  FETCH_PRODUCT_SUCCESS,
  UPDATE_LIST,
} from './fetchTypes';

const initialState = {
  loading: 'false',
  users: [],
  error: '',
};

const fetchReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCT_REQUEST:
      return {
        loading: 'true',
        users: [],
        error: '',
      };

    case FETCH_PRODUCT_SUCCESS: {
      return {
        loading: 'false',
        users: action.payLoad,
        error: '',
      };
    }

    case FETCH_PRODUCT_ERROR: {
      return {
        loading: 'false',
        users: [],
        error: action.payLoad,
      };
    }

    case ADD_TO_LIST: {
      let newState = [...state.users];
      newState.push(action.payLoad);
      return {
        loading: 'false',
        users: newState,
        error: '',
      };
    }

    case UPDATE_LIST: {
      let index = action.payLoad.id - 1;
      let newState = [...state.users];
      newState[index] = action.payLoad;

      return {
        loading: 'false',
        users: newState,
        error: '',
      };
    }

    default:
      return state;
  }
};

export default fetchReducer;
