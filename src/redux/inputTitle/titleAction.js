import { INPUT_TITLE } from './titletypes';

export const inputTitle = (inputValue) => {
  return {
    type: INPUT_TITLE,
    payLoad: inputValue,
  };
};
