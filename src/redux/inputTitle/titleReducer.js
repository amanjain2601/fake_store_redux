import { INPUT_TITLE } from './titletypes';

const initialState = {
  input: '',
};

const titleReducer = (state = initialState, action) => {
  switch (action.type) {
    case INPUT_TITLE:
      return {
        input: action.payLoad,
      };

    default:
      return state;
  }
};

export default titleReducer;
