import { INPUT_CATEGORY } from './categoryTypes';

export const inputCategory = (inputValue) => {
  return {
    type: INPUT_CATEGORY,
    payLoad: inputValue,
  };
};
