import { INPUT_CATEGORY } from './categoryTypes';

const initialState = {
  input: '',
};

const categoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case INPUT_CATEGORY:
      return {
        input: action.payLoad,
      };

    default:
      return state;
  }
};

export default categoryReducer;
