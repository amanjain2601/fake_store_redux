import './App.css';
import DisplayUI from './components/displayUI';
import { store } from './redux/store';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.css';

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <DisplayUI />
      </Provider>
    </div>
  );
}

export default App;
