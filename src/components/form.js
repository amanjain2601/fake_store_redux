import React from 'react';
import { inputTitle } from '../redux/inputTitle/titleAction';
import { inputPrice } from '../redux/inputPrice/priceAction';
import { inputDescription } from '../redux/inputDescription/descriptionAction';
import { inputImage } from '../redux/inputImage/imageAction';
import { inputCategory } from '../redux/inputCategory/categoryAction';
import { addProduct } from '../redux/fetch/fetchAction';
import { updateProduct } from '../redux/fetch/fetchAction';
import SelectID from './selectID';
import { connect } from 'react-redux';

function Form({
  addToList,
  updateProductList,
  inputTitle,
  inputPrice,
  inputDescription,
  inputImageURL,
  inputCategory,
  IDSelected,
  changeTitle,
  changePrice,
  changeDescription,
  changeImageURL,
  changeCategory,
}) {
  function updateFunction(e) {
    e.preventDefault();

    if (!isNaN(IDSelected.input) && IDSelected.input > 0) {
      updateProductList(
        {
          title: inputTitle.input,
          price: inputPrice.input,
          description: inputDescription.input,
          image: inputImageURL.input,
          category: inputCategory.input,
        },
        IDSelected.input
      );
    }
  }

  return (
    <form>
      <SelectID />
      <input
        onInput={(e) => changeTitle(e.target.value)}
        type="text"
        value={inputTitle.input}
        placeholder="Enter a Title"
      ></input>
      <input
        onInput={(e) => changePrice(e.target.value)}
        type="number"
        value={inputPrice.input}
        placeholder="Enter Price"
      ></input>
      <input
        onInput={(e) => changeDescription(e.target.value)}
        type="text"
        value={inputDescription.input}
        placeholder="Enter Description"
      ></input>
      <input
        onInput={(e) => changeImageURL(e.target.value)}
        type="text"
        value={inputImageURL.input}
        placeholder="Enter Image URL"
      ></input>
      <input
        onInput={(e) => changeCategory(e.target.value)}
        type="text"
        value={inputCategory.input}
        placeholder="Enter Category"
      ></input>
      <button
        className="btn btn-warning"
        onClick={(e) => {
          e.preventDefault();
          addToList({
            title: inputTitle.input,
            price: inputPrice.input,
            description: inputDescription.input,
            image: inputImageURL.input,
            category: inputCategory.input,
          });
        }}
      >
        Add
      </button>
      <button className="btn btn-warning" onClick={updateFunction}>
        Update
      </button>
    </form>
  );
}
const mapStateToProps = (state) => {
  return {
    inputTitle: state.title,
    inputPrice: state.price,
    inputDescription: state.description,
    inputImageURL: state.image,
    inputCategory: state.category,
    IDSelected: state.select,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToList: (object) => dispatch(addProduct(object)),
    updateProductList: (object, id) => dispatch(updateProduct(object, id)),
    changeTitle: (value) => dispatch(inputTitle(value)),
    changePrice: (value) => dispatch(inputPrice(value)),
    changeDescription: (value) => dispatch(inputDescription(value)),
    changeImageURL: (value) => dispatch(inputImage(value)),
    changeCategory: (value) => dispatch(inputCategory(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);
