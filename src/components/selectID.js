import { connect } from 'react-redux';
import React from 'react';
import { selectID } from '../redux/selectID/selectAction';

function SelectID({ usersList, changeID }) {
  return (
    <select
      onChange={(e) => changeID(e.target.value)}
      className="select-id-container"
    >
      <option>Select_ID</option>
      {usersList.users.map((current, index) => (
        <option key={index + 1} value={index + 1}>
          {index + 1}
        </option>
      ))}
    </select>
  );
}

const mapStateToProps = (state) => {
  return {
    usersList: state.fetch,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeID: (value) => dispatch(selectID(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectID);
