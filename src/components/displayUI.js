import { connect } from 'react-redux';
import React, { useEffect } from 'react';
import { fetchProducts } from '../redux/fetch/fetchAction';
import SpinnerDisplay from './spinner';
import Form from './form';

function DisplayUI({ fetchList, productsList }) {
  useEffect(() => {
    fetchList();
  }, []);

  if (productsList.loading === true) {
    return <SpinnerDisplay />;
  } else {
    if (productsList.error !== '') {
      return <h1>Error Occured</h1>;
    } else {
      if (productsList.users.length > 0) {
        return (
          <>
            <div className="products-list-container">
              {productsList.users.map((current, index) => (
                <div className="card" key={index}>
                  <div className="product-image">
                    <img
                      src={current.image}
                      className="card-img-top"
                      alt="..."
                    />
                  </div>
                  <div className="card-body">
                    <h3 className="card-title">{current.title}</h3>
                    <h5 className="card-title">{current.price}</h5>
                    <h5 className="card-title">{current.category}</h5>
                    <p className="card-text">{current.description}</p>
                    <button>Read More Here</button>
                  </div>
                </div>
              ))}
            </div>
            <Form />
          </>
        );
      } else {
        return <SpinnerDisplay />;
      }
    }
  }
}

const mapStateToProps = (state) => {
  return {
    productsList: state.fetch,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchList: () => dispatch(fetchProducts()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DisplayUI);
